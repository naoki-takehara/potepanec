require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  describe "カテゴリーページを開く" do
    let!(:product1) { create(:product, name: 'TestRailsTote', price: '10.0', taxons: [taxon1]) }
    let!(:product2) { create(:product, name: 'TestRailsBag', price: '15.0', taxons: [taxon1]) }
    let!(:product3) { create(:product, name: 'TestShirt', price: '20.0', taxons: [taxon2]) }
    let(:taxonomy1) { create(:taxonomy, name: 'TestCategories') }
    let(:taxonomy2) { create(:taxonomy, name: 'TestBrand') }
    let(:taxon1) { create(:taxon, name: 'TestBagsCategory', taxonomy: taxonomy1) }
    let(:taxon2) { create(:taxon, name: 'TestRubyBrand', taxonomy: taxonomy2) }

    before do
      visit potepan_category_path taxon1.id
    end

    it "タイトルが表示される" do
      expect(page).to have_title "#{taxon1.name} - BIGBAG Store"
    end

    it "指定したカテゴリーに属す商品が全て表示される" do
      within ".contentsArea" do
        expect(page).to have_content product1.name
        expect(page).to have_content product1.display_price
        expect(page).to have_content product2.name
        expect(page).to have_content product2.display_price
      end
    end

    it "指定したカテゴリーに属さない商品は表示されない" do
      within ".contentsArea" do
        expect(page).not_to have_content product3.name
        expect(page).not_to have_content product3.display_price
      end
    end

    it "商品をクリックすると商品詳細ページへ移る" do
      within ".contentsArea" do
        expect(page).to have_link product1.name
        click_link product1.name
      end
      expect(current_path).to eq potepan_product_path product1.id
    end

    it "サイドバーにカテゴリー一覧とカテゴリーに属す商品の個数が表示される" do
      within ".sideBar" do
        expect(page).to have_content taxonomy1.name
        expect(page).to have_content taxonomy2.name
        expect(page).to have_content taxon1.name
        expect(page).to have_content taxon2.name
        expect(page).to have_content taxon1.all_products.size
        expect(page).to have_content taxon2.all_products.size
      end
    end

    it "カテゴリーをクリックすると、そのカテゴリーのページへ移る" do
      within ".sideBar" do
        expect(page).to have_link taxon2.name
        click_link taxon2.name
      end
      expect(current_path).to eq potepan_category_path taxon2.id
      within ".contentsArea" do
        expect(page).to have_content product3.name
        expect(page).to have_content product3.display_price
      end
    end

    it "Homeページへのリンクが存在する" do
      expect(page).to have_link "Home"
      click_link "Home", match: :prefer_exact
      expect(current_path).to eq potepan_path
    end
  end
end
