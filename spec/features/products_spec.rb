require 'rails_helper'

RSpec.feature "Products", type: :feature do
  describe "商品詳細ページを開く" do
    let(:taxon1) { create(:taxon, name: 'TestCategory') }
    let(:taxon2) { create(:taxon) }
    let(:product) do
      create(:product, name: 'TestBag',
                       price: '10.0',
                       description: 'This is a test bag',
                       taxons: [taxon1])
    end
    let!(:same_category_product) { create(:product, taxons: [taxon1]) }
    let!(:other_category_product) { create(:product, taxons: [taxon2]) }

    before do
      visit potepan_product_path product.id
    end

    it "タイトルが表示される" do
      expect(page).to have_title "#{product.name} - BIGBAG Store"
    end

    it "商品情報が表示される" do
      within '.singleProduct' do
        expect(page).to have_content product.name
        expect(page).to have_content product.display_price
        expect(page).to have_content product.description
      end
    end

    it "「一覧ページへ戻る」をクリックするとカテゴリー一覧へ移る" do
      expect(page).to have_link "一覧ページへ戻る"
      click_link "一覧ページへ戻る"
      expect(current_path).to eq potepan_category_path(taxon1.id)
      expect(page).to have_content taxon1.name
    end

    it "Homeページへのリンクが存在する" do
      expect(page).to have_link "Home"
      click_link "Home", match: :prefer_exact
      expect(current_path).to eq potepan_path
    end

    it "同じカテゴリーの商品の情報が表示される" do
      within '.productsContent' do
        expect(page).to have_content same_category_product.name
        expect(page).to have_content same_category_product.display_price
      end
    end

    it "違うカテゴリーの商品は表示されない" do
      within '.productsContent' do
        expect(page).not_to have_content other_category_product.name
      end
    end

    it "関連商品をクリックするとその商品の詳細ページへ移る" do
      within '.productsContent' do
        expect(page).to have_link same_category_product.name
        click_link same_category_product.name
        expect(current_path).to eq potepan_product_path same_category_product.id
      end
    end
  end
end
