require 'rails_helper'

RSpec.describe "Categories", type: :request do
  describe "GET #show" do
    let(:taxon) { create(:taxon, name: 'TestCategory') }
    let!(:taxonomy) { create(:taxonomy, name: 'TestCategories') }
    let!(:product) { create(:product, taxons: [taxon]) }

    before do
      get potepan_category_path taxon.id
    end

    it "正常なレスポンスを返す" do
      expect(response.status).to eq 200
    end

    it "showテンプレートが表示される" do
      expect(response).to render_template :show
    end

    it "@taxonを取得できている" do
      expect(response.body).to include taxon.name
    end

    it "@taxonomiesを取得できている" do
      expect(response.body).to include taxonomy.name
    end

    it "@productsを取得できている" do
      expect(response.body).to include product.name
    end
  end
end
