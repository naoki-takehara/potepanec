require 'rails_helper'

RSpec.describe "Products", type: :request do
  describe "GET #show" do
    let(:taxon) { create(:taxon, name: 'TestCategory') }
    let(:product) do
      create(:product, name: 'TestBag',
                       price: '10.0',
                       description: 'This is a test bag',
                       taxons: [taxon])
    end
    let!(:same_category_products) { create_list(:product, 5, taxons: [taxon]) }

    before do
      get potepan_product_path product.id
    end

    it "正常なレスポンスを返す" do
      expect(response.status).to eq 200
    end

    it "showテンプレートが表示される" do
      expect(response).to render_template :show
    end

    it "@productを取得できている" do
      expect(response.body).to include product.name
      expect(response.body).to include product.display_price.to_s
      expect(response.body).to include product.description
    end

    it "@taxonsを取得できている" do
      expect(response.body).to include taxon.id.to_s
    end

    it "関連商品は４つに絞られる" do
      expect(Capybara.string(response.body)).to have_selector ".productBox", count: 4
    end
  end
end
