require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  include ApplicationHelper

  describe "full_titleメソッド" do
    it "引数が空文字列の場合、base_titleを返す" do
      expect(full_title(page_title: "")).to eq "BIGBAG Store"
    end

    it "引数があり空文字列でない場合、引数 - base_titleを返す" do
      expect(full_title(page_title: "TestPage")).to eq "TestPage - BIGBAG Store"
    end

    it "引数がない場合、base_titleを返す" do
      expect(full_title(page_title: nil)).to eq "BIGBAG Store"
    end
  end
end
