require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  describe "related_productsメソッド" do
    let(:taxon1) { create(:taxon, name: 'TestCategory') }
    let(:taxon2) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon1]) }
    let!(:same_category_products) { create_list(:product, 4, taxons: [taxon1]) }
    let(:other_category_product) { create(:product, taxons: [taxon2]) }

    it "呼び出した商品と同じカテゴリーの商品を返す" do
      expect(product.related_products).to eq same_category_products
    end

    it "呼び出した商品自身は返さない" do
      expect(product.related_products).not_to include product
    end

    it "呼び出した商品と違うカテゴリーの商品は返さない" do
      expect(product.related_products).not_to include other_category_product
    end
  end
end
