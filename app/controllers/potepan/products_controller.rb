class Potepan::ProductsController < ApplicationController
  RELATED_PRODUCTS_LIMIT = 4

  def show
    @product = Spree::Product.includes(:taxons).find(params[:id])
    @related_products = @product.related_products.limit(RELATED_PRODUCTS_LIMIT)
    @taxons = @product.taxons
  end
end
